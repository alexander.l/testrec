<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("оформление заказа");

if (CModule::IncludeModule("sale")) {
    function isBasketEmpty()
    {
        $rs_Basket = CSaleBasket::GetList(
            array(),
            array(
                'FUSER_ID' => CSaleBasket::GetBasketUserID(),
                'LID' => SITE_ID,
                'ORDER_ID' => 'NULL',
                'MODULE' => 'catalog'
            ),
            false,
            false,
            array("ID")
        );

        return ($rs_Basket->SelectedRowsCount() > 0);
    }

    ;
}
$a = isBasketEmpty();
if ($a == true) {
    ?>
    <div class="checkout">
        <div class='container'>
            <div class="checkout-tabs">
                <a href="/basket/" class="checkout-tabs__item">Корзина</a>
                <a href="/checkout/" class="checkout-tabs__item active">Оформление заказа</a>
            </div>
            <div class="checkout-content">
                <div class="checkout-column">
                    <div class="checkout-block">
                        <div class="checkout-block__label">Доставка:</div>
                        <div class="checkout-block__value" >Самовывоз <a onclick="changeVal()" class="checkout-block__edit">Изменить</a>

                        </div>
                    </div>
                    <div class="checkout-block">
                        <div class="checkout-block__label">Оплата:</div>
                        <div class="checkout-block__value">При получении картой или наличными</div>
                    </div>
                    <div class="checkout-block">
                        <div class="checkout-block__label">Дополнительная информация:</div>
                        <div class="checkout-block__value" id="asel">Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed
                            do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </div>
                    </div>
                </div>
                <div class="checkout-column">
                    <form method="post" id="order-form" class="checkout-form">
                        <div class="checkout-form-body">
                            <div class="checkout-form__title">Введите Ваши данные</div>
                            <div class="checkout-form-input">
                                <input type="text" name="fio" data-value="Ваше имя" class="input min"/>
                            </div>
                            <div class="checkout-form-input">
                                <input type="text" id="phone" name="phone" placeholder="Телефон*"
                                       class="input req min"/>
                            </div>
                            <div class="checkout-form-input">
                                <input type="text" name="email" data-value="E-mail" class="input email min "/>
                            </div>

                            <div class="checkout-form-address">
                                <div class="checkout-form-address__label">Забрать по адресу</div>
                                <div class="checkout-form-address__value">Москва, Ботаническая 15 (самовывоз)</div>
                            </div>
                            <div class="checkout-form-input">
                                <input type="text" name="comment" data-value="Комментарий к заказу" class="input min"/>
                            </div>
                            <div class="checkout-form-button">
                                <button type="submit" id="order-button" class="form__btn btn-3 min">Оформить заказ
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="checkout__map map" id="map"></div>
        </div>
    </div><? } else {
    ?>
    <div class="textpage">
    <div class="container">
        <div class="textpage__title title cnt"
    </div>
    <div class="textpage-block">

        <div class="textpage__text">
            <p>
                Ваша корзина пуста<a href='/catalog/'> нажмите здесь</a>, чтобы продолжить покупки
            </p>
        </div>
    </div>
    </div>
    </div>
    </div>

<? } ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>