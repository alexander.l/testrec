<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
// sever git test 1
global $USER;
$c = 0;
use \Bitrix\Main,
    \Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context;

$g = 89;
if (!Loader::IncludeModule('sale'))
    die();

function getPropertyByCode($propertyCollection, $code)
{
    foreach ($propertyCollection as $property) {
        if ($property->getField('CODE') == $code)
            return $property;
    }
}

$siteId = \Bitrix\Main\Context::getCurrent()->getSite();

$fio = $_POST['fio'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$delivery = $_POST['deliv'];


$currencyCode = Option::get('sale', 'default_currency', 'RUB');

DiscountCouponsManager::init();

$order = Order::create($siteId, $USER->isAuthorized() ? $USER->GetID() : CSaleUser::GetAnonymousUserID());

$order->setPersonTypeId(1);
$basket = Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), $siteId)->getOrderableItems();

/* Действия над товарами
$basketItems = $basket->getBasketItems();
foreach ($basketItems as $basketItem) {

}
*/

$order->setBasket($basket);
$a = $order->getField('PRICE');
$deliveryPrice = 0;
/*Shipment*/
$shipmentCollection = $order->getShipmentCollection();
$shipment = $shipmentCollection->createItem();
$service = Delivery\Services\Manager::getById($delivery);
if ($delivery == '2') {
    if ($a > 20000) {
        $deliveryPrice = 0;
    } else {

        $deliveryPrice = 300;
    }
};
$shipment->setFields(array(
    'DELIVERY_ID' => $delivery,
    'DELIVERY_NAME' => $service['NAME'],
    'ALLOW_DELIVERY' => 'Y',
    'PRICE_DELIVERY' => $deliveryPrice,
    'CUSTOM_PRICE_DELIVERY' => 'Y'
));
$shipmentItemCollection = $shipment->getShipmentItemCollection();
foreach ($order->getBasket() as $item) {
    $shipmentItem = $shipmentItemCollection->createItem($item);
    $shipmentItem->setQuantity($item->getQuantity());
}

/*Payment*/
$paymentCollection = $order->getPaymentCollection();
$payment = $paymentCollection->createItem();
$paySystemService = PaySystem\Manager::getObjectById(2); //обычно 1 - это оплата наличными
$payment->setFields(array(
    'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
    'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
));
/**/

$order->doFinalAction(true);
$propertyCollection = $order->getPropertyCollection();

$emailProperty = getPropertyByCode($propertyCollection, 'EMAIL');
$emailProperty->setValue($email);

$phoneProperty = getPropertyByCode($propertyCollection, 'PHONE');
$phoneProperty->setValue($phone);
$b = $order->getField('PRICE');
$fioProperty = getPropertyByCode($propertyCollection, 'FIO');
$fioProperty->setValue($fio);

$order->setField('CURRENCY', $currencyCode);
$order->setField('USER_DESCRIPTION', $_POST['comment']);
$order->setField('COMMENTS', $_POST['deliv']);

$order->save();


$orderId = $order->GetId();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");