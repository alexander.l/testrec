<?
//Подключаем ядро Битрикс и главный модуль
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

//Подключаем модуль sale
Loader::includeModule("sale");

$PRODUCT_ID = $_POST['p_iidd'];
if (CModule::IncludeModule("catalog")) {

    if (CSaleBasket::Delete($PRODUCT_ID)) {
        echo "Запись успешно удалена";
    }


}
//Выводи количество отложенных товаров

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>