

            <?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';
$strReturn .= '<div class="bx-breadcrumb" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0? '<span class="breadcrumbs__sepp">|</span>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '
			
		
            <a href="'.$arResult[$index]["LINK"].'" class="breadcrumbs__link">'.$title.'</a>
            <span class="breadcrumbs__sepp">'.$arrow.'</span>

';


	}
	else
	{
		$strReturn .= '
			<a  class="breadcrumbs__item">'.$title.'</a>';
	}
}

$strReturn .= '<div style="clear:both"></div></div>';

return $strReturn;?>

