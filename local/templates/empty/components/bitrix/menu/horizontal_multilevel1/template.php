<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class='container'>
        <div class="header-menu">
            <ul class="header-menu-list">
                <? foreach ($arResult as $arItem) { ?>
                    <? if ($arItem['PARAMS']["DEPTH_LEVEL"] == 1) { ?>
                        <li><a href="<?= $arItem["LINK"] ?>" class="header-menu__link"><?= $arItem['TEXT'] ?></a></li>
                    <? }
                }; ?>
                <li><span class="header-menu-more">Ещё <span
                                class="header-menu-more__icon"><span></span><span></span><span></span></span></span>
                </li>
            </ul>
        </div>
    </div>
    <div class="header-submenu">
        <div class='container'>
            <div class="header-submenu-body">
                <div class="header-submenu-column">
                    <div class="header-submenu__label">Коллекции</div>
                    <ul class="header-submenu-list">
                        <li><a href="/catalog/" class="header-submenu__link">Коллекция 1</a></li>
                        <li><a href="/catalog/" class="header-submenu__link">Коллекция 2</a></li>
                        <li><a href="/catalog/" class="header-submenu__link">Коллекция 3</a></li>
                        <li><a href="/catalog/" class="header-submenu__link">Коллекция 4</a></li>
                        <li><a href="/catalog/" class="header-submenu__link">Коллекция 5</a></li>
                        <li><a href="/catalog/" class="header-submenu__link">Коллекция 6</a></li>
                        <li><a href="/catalog/" class="header-submenu__link">Коллекция 7</a></li>
                    </ul>
                </div>
                <div class="header-submenu-column">
                    <div class="header-submenu__label">Каталог</div>
                    <ul class="header-submenu-list">
                        <? foreach ($arResult as $arItem) { ?>
                            <? if ($arItem['PARAMS']["DEPTH_LEVEL"] == 2) { ?>
                                <li><a href="<?= $arItem["LINK"] ?>"
                                       class="header-submenu__link"><?= $arItem['TEXT'] ?></a></li>
                            <? }
                        }; ?>
                    </ul>
                </div>
                <div class="header-submenu-column">
                    <div class="header-submenu__label">Компания</div>
                    <ul class="header-submenu-list">
                        <? foreach ($arResult as $arItem) { ?>
                            <? if ($arItem['PARAMS']["DEPTH_LEVEL"] == 3) { ?>
                                <li><a href="<?= $arItem["LINK"] ?>"
                                       class="header-submenu__link"><?= $arItem['TEXT'] ?></a></li>
                            <? }
                        }; ?>
                    </ul>
                    <div class="header-submenu-social">
                        <a href="<? $APPLICATION->includeFile(
                            SITE_DIR . "include/vk.php",
                            array(),
                            array()
                        ) ?>" class="header-submenu-social__item fa fa-vk"></a>
                        <a href="<? $APPLICATION->includeFile(
                            SITE_DIR . "include/facebook.php",
                            array(),
                            array()
                        ) ?>" class="header-submenu-social__item fa fa-facebook"></a>
                        <a href="<? $APPLICATION->includeFile(
                            SITE_DIR . "include/instagram.php",
                            array(),
                            array()
                        ) ?>" class="header-submenu-social__item fa fa-instagram"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<? endif ?>
