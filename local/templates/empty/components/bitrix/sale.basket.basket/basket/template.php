<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$a = 0;

use Bitrix\Main;
use Bitrix\Main\Localization\Loc; ?>
<? if ($arResult["BASKET_ITEMS_COUNT"] != 0) { ?>
    <div class="cart">
        <div class='container'>
            <div class="cart-tabs">
                <a href="/basket/" class="cart-tabs__item active">Корзина</a>
                <a href="/checkout/" class="cart-tabs__item">Оформление заказа</a>
            </div>
            <div class="cart-content">
                <div class="cart-body">
                    <div class="cart-items">
                        <? foreach ($arResult["GRID"]["ROWS"] as $arItem) {
                            $a = $arItem['SUM_VALUE'] ?>
                            <div class="cart-item">
                                <div class="cart-item-product">
                                    <a href="" class="cart-item-product__image"><img
                                                src="<?= $arItem["PREVIEW_PICTURE_SRC"] ?>" alt=""/></a>
                                    <div class="cart-item-product-body">
                                        <a href="" class="cart-item-product__title"><?= $arItem['NAME'] ?></a>
                                        <div class="cart-item-product__label"><?= $arItem["PROPERTY_ATT_COLACTIONN_VALUE"] ?></div>
                                        <div class="cart-item-product__option">
                                            <strong>Камень:</strong> <?= $arItem["PROPERTY_ATT_STONE_VALUE"] ?></div>
                                    </div>
                                </div>
                                <div class="cart-item__price"><?= ($arItem["BASE_PRICE"]) ?></div>
                                <a href="javascript:void(0)" onclick="delCart('<?= $arItem["ID"] ?>',this)"
                                   class="cart-item__delete fa fa-times"></a>
                            </div>
                        <? } ?>
                    </div>
                </div>
                <div class="cart-info">
                    <div class="cart-info-table table">
                        <div class="trow">
                            <div class="cell">
                                <div class="cart-info__label">Товаров</div>
                            </div>
                            <div class="cell">
                                <div class="cart-info__value"><?= $arResult["BASKET_ITEMS_COUNT"] ?></div>
                            </div>
                        </div>
                        <div class="trow">
                            <div class="cell">
                                <div class="cart-info__label">На сумму</div>
                            </div>
                            <div class="cell">
                                <div class="cart-info__value"><?= $arResult ["allSum_FORMATED"]; ?></div>
                            </div>
                        </div>
                        <div class="trow">
                            <div class="cell">
                                <div class="cart-info__label">Доставка</div>
                            </div>
                            <div class="cell">
                                <div class="cart-info__value">
                                    <? if ($a > 20000) { ?>
                                        <span>300 ₽</span> 0 ₽
                                    <? } else {
                                        $a += 300;

                                        ?>
                                        <span></span> 300 ₽
                                    <? } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cart-info-total">
                        <div class="cart-info-total__label">Всего к оплате</div>
                        <div class="cart-info-total__value"><?= $a ?> ₽</div>
                    </div>
                </div>
            </div>
            <div class="cart-actions">
                <div class="cart-actions-column">
                    <div class="cart-actions-delivery">
                        <div class="cart-actions-delivery__label">Доставка</div>
                        <select id="select12" name="form[]" class="cart-actions-delivery">
                            <option value="3" selected="selected">Самовывоз</option>
                            <option value="2">Курьером</option>
                        </select>
                    </div>
                    <div class="cart-actions-footer">
                        <div class="cart-actions__info">При покупке на 20 000 рублей и более — бесплатная доставка по
                            Москве!
                        </div>
                    </div>
                </div>
                <div class="cart-actions-column">
                    <a href="/checkout/" id="select21" class="cart-actions__btn btn-3 min">Далее</a>
                </div>
            </div>
        </div>
    </div>
<? } else { ?>
<div class="textpage">
    <div class="container">
        <div class="textpage__title title cnt"
    </div>

    <div class="textpage-block">

        <div class="textpage__text">
            <p>
                <?= Loc::getMessage("SBB_EMPTY_BASKET_TITLE");
                echo Loc::getMessage("SBB_EMPTY_BASKET_HINT");
                } ?>
            </p>

        </div>
    </div>
</div>
</div>
</div>


<script type="text/javascript">
    function delCart(p_iidd) {
        $.ajax({
            type: "POST",
            url: "/ajax/cart-ajax-del.php",
            data: "p_iidd=" + p_iidd,
            success: function () {
                location.reload()
            }
        });
    };
</script>