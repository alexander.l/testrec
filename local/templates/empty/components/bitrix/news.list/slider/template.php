<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<div class="mainslider">
    <div class="mainslider-slider">
        <? $UTP = $arResult["DETAIL_TEXT"] ?>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>


            <div class="mainslide">
                <div class="mainslide-content">
                    <div class='container'>
                        <div class="mainslide-body">
                            <div class="mainslide__title"><? echo $arItem["NAME"] ?></div>
                            <div class="mainslide__text"><? echo $arItem["PREVIEW_TEXT"]; ?></div>
                            <a href="/catalog/" class="mainslide__btn btn-3">Смотреть</a>
                        </div>
                    </div>
                    <div class="mainslide__bg ibg"
                         style="background-image: url(<? echo $arItem["PREVIEW_PICTURE"]["SRC"] ?>)"></div>
                </div>
            </div>
            <div class="mainslider-arrows">
                <div class='container'></div>
            </div>

        <? endforeach; ?>
        <div class="mainline">
            <div class='container'>
                <div class="mainline__text"><?= $UTP ?>111</div>
            </div>
        </div>
    </div>
</div>