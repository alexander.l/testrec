<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="catalog-slider">
    <div class="catalog-slider__title title cnt"><? echo GetMessage("CT_BNL_ELEMENT_DELETE_SEE") ?></div>
    <div class="product-slider-body">

        <? $i = 0;
        if ($i = 5) {
            $i = 0;
        };
        foreach ($arResult["ITEMS"] as $cell => $arItem) {
            switch ($i) {
                case 0:
                    ?>
                    <div class="catalog-slide">
                        <div class="catalog-item white_1">
                            <div class="catalog-item-content">
                                <a href="" class="catalog-item-content__image ibg"><img
                                            src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt=""/></a>
                                <a href="" class="catalog-item-content__text">
                                    <span><? echo $arItem["NAME"] ?></span>
                                    <span><? echo($arItem["PRICE"]) ?> ₽</span>
                                </a>
                            </div>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="catalog-item-hover">
                                <span class="catalog-item-hover__btn btn-4"><? echo GetMessage("CT_BNL_ELEMENT_DELETE_VIEW") ?></span>
                            </a>
                        </div>
                    </div>

                    <? break;
                case 1:
                    ?>
                    <div class="catalog-slide">
                        <div class="catalog-item black">
                            <div class="catalog-item-content">
                                <a href="" class="catalog-item-content__image ibg"><img
                                            src="<? echo($arItem["ATT_BLACK_PHOTO"]) ?>" alt=""/></a>
                                <a href="" class="catalog-item-content__text">
                                    <span><? echo $arItem["NAME"] ?></span>
                                    <span><? echo($arItem["PRICE"]) ?> ₽</span>
                                </a>
                            </div>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="catalog-item-hover">
                                <span class="catalog-item-hover__btn btn-4"><? echo GetMessage("CT_BNL_ELEMENT_DELETE_VIEW") ?></span>
                            </a>
                        </div>
                    </div>
                    <? break;
                case 2:
                    ?>
                    <div class="catalog-slide">
                        <div class="catalog-item white">
                            <div class="catalog-item-content">
                                <a href="" class="catalog-item-content__image ibg"><img
                                            src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt=""/></a>
                                <a href="" class="catalog-item-content__text">
                                    <span><? echo $arItem["NAME"] ?></span>
                                    <span><? echo($arItem["PRICE"]) ?> ₽</span>
                                </a>
                            </div>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="catalog-item-hover">
                                <span class="catalog-item-hover__btn btn-4"><? echo GetMessage("CT_BNL_ELEMENT_DELETE_VIEW") ?></span>
                            </a>
                        </div>
                    </div>
                    <? break;
                case 3:
                    ?>

                    <div class="catalog-slide">
                    <div class="catalog-item white">
                        <div class="catalog-item-content">
                            <a href="" class="catalog-item-content__image ibg"><img
                                        src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt=""/></a>
                            <a href="" class="catalog-item-content__text">
                                <span><? echo $arItem["NAME"] ?></span>
                                <span><? echo($arItem["PRICE"]) ?> ₽</span>
                            </a>
                        </div>
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="catalog-item-hover">
                            <span class="catalog-item-hover__btn btn-4"><? echo GetMessage("CT_BNL_ELEMENT_DELETE_VIEW") ?></span>
                        </a>
                    </div>
                    </div><?
                    break;
            }
            $i++;
        };

        ?>
    </div>
</div>
</div>