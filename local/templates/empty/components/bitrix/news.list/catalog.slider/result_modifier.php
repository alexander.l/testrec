<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(CModule::IncludeModule("catalog")) {
  $CURENT_ITEM_ID = 0;
  
  foreach($arResult["ITEMS"] as $item_){
    $product =  CPrice::GetBasePrice($item_["ID"]);
    $arResult["ITEMS"][$CURENT_ITEM_ID ]["PRICE"] = $product ["PRICE"];
    //------------------
    $db_props = CIBlockElement::GetProperty($item_["IBLOCK_ID"], $item_["~ID"], array("sort" => "asc"), Array("CODE"=>"ATT_BLACK_PHOTO"));
	$ar_props = $db_props->Fetch();
  	$ATT_PHOTO = $ar_props["VALUE"];				
	$ATT_BLACK_PHOTO = CFile::GetPath( $ATT_PHOTO);
	$arResult["ITEMS"][$CURENT_ITEM_ID ]["ATT_BLACK_PHOTO"] = $ATT_BLACK_PHOTO;
	//------------------
	$CURENT_ITEM_ID++;		  	  
  }
}
unset($item_);
unset($product);
unset($CURENT_ITEM_ID );