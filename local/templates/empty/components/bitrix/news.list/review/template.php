<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="clients">
    <div class="clients-header">
		<div class='container'>
			<div class="clients__title"><?echo GetMessage("CT_BNL_CLIENTS_TITLE")?></div>
		</div>
		</div>	
		<div class="clients-items">
<?foreach($arResult["ITEMS"] as $arItem):?>
	
				<div class="clients-item">
					<div class="clients-item__bg ibg" style="background-image: url(<?echo $arItem["PREVIEW_PICTURE"]["SRC"]?>)">
					     
				    </div>
					<div class="clients-item-content">
				    <div class="clients-item-content__title"><?echo $arItem["NAME"]?></div>
					<div class="clients-item-content__text"><?echo $arItem["PREVIEW_TEXT"];?></div>												
					     <a href="/catalog/" class="clients-item-content__btn btn-4"><?echo GetMessage("CT_BNL_BTN_4")?></a>
				    </div>
				</div>

<?endforeach;?>
</div>
</div>
</div>


</div>	