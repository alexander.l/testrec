<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (CModule::IncludeModule("catalog")) {
    $filter_7 = array('IBLOCK_ID' => 6,);
    $db_list_id7 = CIBlockSection::GetList(array(), $filter_7);
    $arSect = array();
    while ($obb = $db_list_id7->GetNextElement()) {
        $arSect[] = $obb->GetFields();
    }

    foreach ($arSect as $cell => $sc) {
        unset($ar);
        $arSelect = array("ID", 'IBLOCK_ID', "NAME", "DATE_ACTIVE_FROM", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "PREVIEW_PICTURE", "CANONICAL_PAGE_URL", "CATALOG_GROUP_1");
        $arFilter = array('IBLOCK_SECTION_ID' => $sc['ID']);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);

        while ($ob = $res->GetNextElement()) {
            $ar[] = $ob->GetFields();
            if ($ar != null) {
                $sc["PROD"] = $ar;


                $arResult['CATEGORY_MERGE'][$sc['NAME']]["PROD"] = $ar;
                $arResult['CATEGORY_MERGE'][$sc['NAME']]["PROD"]['NAMES'] = $sc['NAME'];

            }
        };

    };


}