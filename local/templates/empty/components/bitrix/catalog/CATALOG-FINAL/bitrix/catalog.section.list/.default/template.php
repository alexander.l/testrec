<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>



    <div class="filter-modules">
        <div class="filter-module">
            <div class="filter__label">Категории</div>
            <ul class="filter-menu">


                <? $TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
                $CURRENT_DEPTH = $TOP_DEPTH;

                foreach ($arResult["SECTIONS"] as $arSection):
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'))); ?>
                    <li id="<?= $this->GetEditAreaId($arSection['ID']); ?>">
                        <a class="filter-menu__link"
                           href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?><span
                                    class="fa fa-angle-down"></span>
                            <ul class="filter-submenu">

                                <li><a href="" class="filter-submenu__link">Коллекция 1</a></li>
                                <li><a href="" class="filter-submenu__link">Коллекция 2</a></li>
                                <li><a href="" class="filter-submenu__link">Коллекция 3</a></li>
                                <li><a href="" class="filter-submenu__link">Коллекция 4</a></li>
                                <li><a href="" class="filter-submenu__link">Коллекция 5</a></li>
                            </ul>
                    </li>
                <? endforeach; ?>

        </div>


    </div>

				