<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
foreach ($arResult['CATEGORY_MERGE'] as $sc){?>
<div class="catalog">
    <div class="catalog__title cnt title"><?= ($sc['PROD']['NAMES']) ?></div>
    <div class="catalog-items">
        <div class="catalog-items-sector"></div>
        <div class="catalog-items-block">
            <div class="catalog-item ">
                <div class="catalog-item-content">
                    <a href="" class="catalog-item-content__image ibg"><img
                                src="<?= CFile::GetPath($sc['PROD'][0]["DETAIL_PICTURE"]); ?>" alt=""/></a>
                    <a href="" class="catalog-item-content__text">
                        <span><?= $sc['PROD'][0]["NAME"] ?></span>
                        <span> <?= $sc['PROD'][0]["CATALOG_PRICE_1"]?></span>
                    </a>
                </div>
                <a href="<?= $sc["PROD"][0]["DETAIL_PAGE_URL"] ?>" class="catalog-item-hover">
                    <span class="catalog-item-hover__btn btn-4"><?= GetMessage('CT_BCS_VIEW') ?></span>
                </a>
            </div>
            <div class="catalog-item white_2">
                <div class="catalog-item-content">
                    <a href="" class="catalog-item-content__image ibg"><img
                                src="<?= CFile::GetPath($sc['PROD'][0]["DETAIL_PICTURE"]); ?>" alt=""/></a>
                    <a href="" class="catalog-item-content__text">
                        <span><?= $sc['PROD'][1]["NAME"] ?></span>
                        <span> <?= $sc['PROD'][1]["CATALOG_PRICE_1"]?></span>
                    </a>
                </div>
                <a href="<?= $sc["PROD"][1]["DETAIL_PAGE_URL"] ?>" class="catalog-item-hover">
                    <span class="catalog-item-hover__btn btn-4"><?= GetMessage('CT_BCS_VIEW') ?></span>
                </a>
            </div>
            <div class="catalog-item white">
                <div class="catalog-item-content">
                    <a href="" class="catalog-item-content__image ibg"><img
                                src="<?= CFile::GetPath($sc['PROD'][0]["DETAIL_PICTURE"]); ?>" alt=""/></a>
                    <a href="" class="catalog-item-content__text">
                        <span><?= $sc['PROD'][2]["NAME"] ?></span>
                        <span><?= $sc['PROD'][2]["CATALOG_PRICE_1"]?></span>
                    </a>
                </div>
                <a href="<?= $sc['PROD'][2]["DETAIL_PAGE_URL"] ?>" class="catalog-item-hover">
                    <span class="catalog-item-hover__btn btn-4"><?= GetMessage('CT_BCS_VIEW') ?></span>
                </a>
            </div>
        </div>
        <div class="catalog-items-button">
            <a href="<?= ($sc['PROD'][0]["CANONICAL_PAGE_URL"]) ?>" class="catalog-items__btn btn-3">Посмотреть
                все <?= ($sc['PROD']['NAMES']) ?></a>

							   
             <?}?>
       