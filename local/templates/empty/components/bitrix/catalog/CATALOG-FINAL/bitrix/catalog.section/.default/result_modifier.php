<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (CModule::IncludeModule("catalog")) {
    $ar = [];
    $acol = '';
    $_id = 0;
    foreach ($arResult["ITEMS"] as $cell => $arElement) {
        $acol = $arElement['PROPERTIES']["ATT_COLACTIONN"]["VALUE"];
        unset($ar);
        $ar[] = $arElement;
        $arResult['COLACTION_GROUP'][$acol]['CAT_NAME'] = $acol;
        $arResult['COLACTION_GROUP'][$acol]['ITEMS'][$_id] = $ar;

        $_id++;
    };
}
?>