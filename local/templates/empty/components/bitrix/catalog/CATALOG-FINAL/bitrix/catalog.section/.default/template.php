<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="content-block">
    <div class="catalog">
        <div class="catalog__title cnt title"><?= $arResult["NAME"] ?></div>
        <div class="catalog-items">
            <div class="catalog-items-sector">


                <? $ci = 0;
                foreach ($arResult['COLACTION_GROUP'] as $aritem) { ?>
                    <div class="catalog-items-sector__subtitle">«<?= ($aritem['CAT_NAME']) ?>»</div>
                    <div class="catalog-items-block">

                        <? foreach ($aritem['ITEMS'] as $ar) { ?>

                            <div class="catalog-item ">
                                <div class="catalog-item-content">
                                    <a href="" class="catalog-item-content__image ibg"><img
                                                src="<?= $ar[0]["DETAIL_PICTURE"]["SRC"] ?>" alt=""/></a>
                                    <a href="" class="catalog-item-content__text">
                                        <span><? echo($ar[0]['NAME']) ?></span>
                                        <span><? echo($ar[0]["PRICES"]["BASE"]["VALUE"]) ?> ₽</span>
                                    </a>
                                </div>
                                <a href="<? echo($ar[0]['DETAIL_PAGE_URL']) ?>" class="catalog-item-hover">
                                    <span class="catalog-item-hover__btn btn-4">Смотреть</span>
                                </a>
                            </div>
                        <?$ci++; if ($ci == 3){break; } }?>

                    </div>
                    <div class="catalog-items-button">
                        <a href="" class="catalog-items__btn btn-3">Посмотреть все</a>
                    </div>
                <? } ?>
            </div>
        </div>
    </div>
</div>
</div>
			
