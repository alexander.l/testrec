<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


    <div class="product">
    <div class='container'>
        <div class="product-main">
            <div class="product-main-images">
                <div class="product-main-mainimages row">

                    <? foreach ($arResult["PROPERTIES"]["ATT_MORE_PHOTO"]['VALUE'] as $arPhoto) {
                        $photo = CFile::GetPath($arPhoto); ?>
                        <div class="product-main-mainimage">
                            <a rel="position:'right',adjustX:25,adjustY:0,Width: 432" href="<?= $photo ?>"
                               class="cloud-zoom product-main-mainimage__item">
                                <img class="cloudzoom-gallery" src="<?= $photo ?>" alt=""/>
                            </a>
                        </div>
                    <? } ?>
                </div>
                <div class="product-main-subimages">
                    <? foreach ($arResult["PROPERTIES"]["ATT_MORE_PHOTO"]['VALUE'] as $arPhoto) {
                        $photo = CFile::GetPath($arPhoto); ?>
                        <div class="product-main-subimage">
                            <div class="product-main-subimage__item ibg"><img src="<?= $photo ?>" alt=""/></div>
                        </div>
                    <? } ?>

                </div>
            </div>

            <div class="product-main-info">


                <div class="product-main-info__title"><?= $arResult['NAME'] ?></div>
                <div class="product-main-info__price"><?= $arResult['PRICES']['BASE']['VALUE']; ?> ₽
                </div>
                <div class="product-main-info-options">
                    <p><strong><? if ($arResult["PROPERTIES"]["ATT_MODEL"]["VALUE"] != "") {
                                echo("Модель:");
                            } ?></strong><? echo(" " . $arResult["PROPERTIES"]["ATT_MODEL"]["VALUE"]) ?></p>
                    <p><strong>    <? if ($arResult["PROPERTIES"]["ATT_STONE"]["VALUE"] != "") {
                                echo("Камень:");
                            } ?></strong><? echo(" " . $arResult["PROPERTIES"]["ATT_STONE"]["VALUE"][0]) ?></p>


                </div>
                <a href="javascript:void(0)" id="btn0603" class="product-main-info__cart pl incart min btn-3"
                   onclick="add2cart('<?= $arResult["ID"] ?>',this);">В корзину</a>
            </div>
        </div>
        <div class="product-info">
            <div class="product-info-table table">
                <div class="trow">
                    <div class="cell">
                        <div class="product-info__label">
                            <? if ($arResult["PROPERTIES"]["ATT_METAL"]["VALUE"] != "") {
                                echo("Металл:");
                            } ?>
                        </div>
                    </div>
                    <div class="s">
                        <div class="product-info__value"><? echo($arResult["PROPERTIES"]["ATT_METAL"]["VALUE"][0]) ?></div>
                    </div>
                </div>
                <div class="trow">
                    <div class="cell">
                        <div class="product-info__label">
                            <? if ($arResult["PROPERTIES"]["ATT_PROP"]["VALUE"] != "") {
                                echo("Характеристики:");
                            } ?>
                        </div>
                    </div>
                </div>
                <div class="cell">
                    <div class="product-info__value"><? echo($arResult["PROPERTIES"]["ATT_PROP"]["VALUE"]) ?></div>
                </div>
            </div>
        </div>
        <div class="product-info-description">
            <div class="product-info__title">
                <? if ($arResult["DETAIL_TEXT"] != "") {
                    echo("Описание:");
                } ?>
            </div>
            <div class="product-info__text">  <? echo($arResult["DETAIL_TEXT"]) ?>
            </div>
        </div>
    </div>

<? $arResult['VAR_Y'] = $arResult["PREVIEW_PICTURE"]["SRC"];
$this->__component->setResultCacheKeys(array('VAR_Y')); ?>
<? $arResult['VAR_X'] = $arResult["NAME"];
$this->__component->setResultCacheKeys(array('VAR_X')); ?>