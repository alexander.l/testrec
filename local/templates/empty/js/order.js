
var count = 2;

function changeVal() {
    if (count === 2) {
        $('#deliv12').html('Самовывоз <a onclick="changeVal()" class="checkout-block__edit">Изменить</a>');
        console.log('2');
        count++;
    } else {
        $('#deliv12').html('Курьером <a onclick="changeVal()" class="checkout-block__edit">Изменить</a>');
        console.log('3');
        count = 2;
    }
}

$('#order-button').click(function (event) {
    var phone = $('#phone').val();
    if (phone == '') {
        $('#phone').addClass("err");
        event.preventDefault();
    } else {
        $.ajax({
                type: 'POST',
                url: '/ajax/checkout-order.php',
                data: $('#order-form').serialize() + '&deliv=' + count,
                dataType: 'json',
                success: function (data) {
                    alert(data);
                }
            }
        );

    }

});
